# frozen_string_literal: true

module QontakFcm
  class APIError < StandardError; end
  class NetworkError < APIError; end

  class HttpError < APIError
    attr_reader :response

    def initialize(message, response)
      super(message)
      @response = response
    end
  end

  class ClientError < HttpError; end

  class BadRequest                  < ClientError; end # rubocop:disable   rubocop:disable  status: 400
  class Unauthorized                < ClientError; end # rubocop:disable   rubocop:disable  status: 401
  class PaymentRequired             < ClientError; end # rubocop:disable   rubocop:disable  status: 402
  class Forbidden                   < ClientError; end # rubocop:disable   rubocop:disable  status: 403
  class NotFound                    < ClientError; end # rubocop:disable   rubocop:disable  status: 404
  class MethodNotAllowed            < ClientError; end # rubocop:disable   rubocop:disable  status: 405
  class NotAcceptable               < ClientError; end # rubocop:disable   rubocop:disable  status: 406
  class ProxyAuthenticationRequired < ClientError; end # rubocop:disable   rubocop:disable  status: 407
  class RequestTimeout              < ClientError; end # rubocop:disable   rubocop:disable  status: 408
  class Conflict                    < ClientError; end # rubocop:disable   rubocop:disable  status: 409
  class Gone                        < ClientError; end # rubocop:disable   rubocop:disable  status: 410
  class LengthRequired              < ClientError; end # rubocop:disable   rubocop:disable  status: 411
  class PreconditionFailed          < ClientError; end # rubocop:disable   rubocop:disable  status: 412
  class PayloadTooLarge             < ClientError; end # rubocop:disable   rubocop:disable  status: 413
  class URITooLong                  < ClientError; end # rubocop:disable   rubocop:disable  status: 414
  class UnsupportedMediaType        < ClientError; end # rubocop:disable   rubocop:disable  status: 415
  class RangeNotSatisfiable         < ClientError; end # rubocop:disable   rubocop:disable  status: 416
  class ExpectationFailed           < ClientError; end # rubocop:disable   rubocop:disable  status: 417
  class ImaTeapot                   < ClientError; end # rubocop:disable   rubocop:disable  status: 418
  class MisdirectedRequest          < ClientError; end # rubocop:disable   rubocop:disable  status: 421
  class UnprocessableEntity         < ClientError; end # rubocop:disable   rubocop:disable  status: 422
  class Locked                      < ClientError; end # rubocop:disable   rubocop:disable  status: 423
  class FailedDependency            < ClientError; end # rubocop:disable   rubocop:disable  status: 424
  class UpgradeRequired             < ClientError; end # rubocop:disable   rubocop:disable  status: 426
  class PreconditionRequired        < ClientError; end # rubocop:disable   rubocop:disable  status: 428
  class TooManyRequests             < ClientError; end # rubocop:disable   rubocop:disable  status: 429
  class RequestHeaderFieldsTooLarge < ClientError; end # rubocop:disable   rubocop:disable  status: 431
  class UnavailableForLegalReasons  < ClientError; end # rubocop:disable   rubocop:disable  status: 451

  class ServerError < HttpError; end

  class InternalServerError           < ServerError; end # rubocop:disable   rubocop:disable  status: 500
  class NotImplemented                < ServerError; end # rubocop:disable   rubocop:disable  status: 501
  class BadGateway                    < ServerError; end # rubocop:disable   rubocop:disable  status: 502
  class ServiceUnavailable            < ServerError; end # rubocop:disable   rubocop:disable  status: 503
  class GatewayTimeout                < ServerError; end # rubocop:disable   rubocop:disable  status: 504
  class HTTPVersionNotSupported       < ServerError; end # rubocop:disable   rubocop:disable  status: 505
  class VariantAlsoNegotiates         < ServerError; end # rubocop:disable   rubocop:disable  status: 506
  class InsufficientStorage           < ServerError; end # rubocop:disable   rubocop:disable  status: 507
  class LoopDetected                  < ServerError; end # rubocop:disable   rubocop:disable  status: 508
  class NotExtended                   < ServerError; end # rubocop:disable   rubocop:disable  status: 510
  class NetworkAuthenticationRequired < ServerError; end # rubocop:disable   rubocop:disable  status: 511

  STATUS_TO_EXCEPTION_MAPPING = {
    '400' => BadRequest,
    '401' => Unauthorized,
    '402' => PaymentRequired,
    '403' => Forbidden,
    '404' => NotFound,
    '405' => MethodNotAllowed,
    '406' => NotAcceptable,
    '407' => ProxyAuthenticationRequired,
    '408' => RequestTimeout,
    '409' => Conflict,
    '410' => Gone,
    '411' => LengthRequired,
    '412' => PreconditionFailed,
    '413' => PayloadTooLarge,
    '414' => URITooLong,
    '415' => UnsupportedMediaType,
    '416' => RangeNotSatisfiable,
    '417' => ExpectationFailed,
    '418' => ImaTeapot,
    '421' => MisdirectedRequest,
    '422' => UnprocessableEntity,
    '423' => Locked,
    '424' => FailedDependency,
    '426' => UpgradeRequired,
    '428' => PreconditionRequired,
    '429' => TooManyRequests,
    '431' => RequestHeaderFieldsTooLarge,
    '451' => UnavailableForLegalReasons,
    '500' => InternalServerError,
    '501' => NotImplemented,
    '502' => BadGateway,
    '503' => ServiceUnavailable,
    '504' => GatewayTimeout,
    '505' => HTTPVersionNotSupported,
    '506' => VariantAlsoNegotiates,
    '507' => InsufficientStorage,
    '508' => LoopDetected,
    '510' => NotExtended,
    '511' => NetworkAuthenticationRequired
  }.freeze

  class ResponseHandler
    def received_response(response, _options)
      error = STATUS_TO_EXCEPTION_MAPPING[response.code]
      raise error.new("Receieved an error response #{response.code} #{error.to_s.split('::').last}: #{response.body}", response) if error

      response
    end
  end

  private_constant :ResponseHandler
end
