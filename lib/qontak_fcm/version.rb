# frozen_string_literal: true

module QontakFcm
  VERSION = '0.0.0'
  public_constant :VERSION
end
