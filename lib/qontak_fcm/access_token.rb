module QontakFcm
  class AccessToken
    require "googleauth"
    attr_reader :access_token

    def initialize(gdc_path, redis_cache)
      @gdc_path     = gdc_path
      @redis_cache  = redis_cache
      @access_token = ""
      check_access_token
    end

    def fetch_access_token
      authorizer = Google::Auth::ServiceAccountCredentials.make_creds(
          json_key_io: File.open(@gdc_path),
          scope:       "https://www.googleapis.com/auth/firebase.messaging",
      )
      authorizer.fetch_access_token!
      @redis_cache.hset('fcm_access_token', 'FcmAccessToken', { :access_token => authorizer.access_token, :expires_at => authorizer.expires_at.to_i }.to_json) rescue nil
      @access_token = authorizer.access_token
    end

    def check_access_token
      fcm_access_token = JSON.parse(@redis_cache.hget('fcm_access_token', 'FcmAccessToken')).with_indifferent_access rescue nil
      if fcm_access_token.present?
        if DateTime.now.to_i > fcm_access_token[:expires_at]
          fetch_access_token
        else
          @access_token = fcm_access_token[:access_token]
        end
      else
        fetch_access_token
      end
      @access_token
    end
  end
end