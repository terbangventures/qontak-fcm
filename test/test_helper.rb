# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'qontak_fcm'
require 'minitest/autorun'
