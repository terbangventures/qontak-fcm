## Installation

Add this line to your application's Gemfile:

```ruby
gem 'qontak-fcm', bitbucket: 'terbangventures/qontak-fcm/src/master'
```

Or install it yourself as:

    $ gem install qontak-fcm

## Usage

You'll need your application's server key, whose value is available in the [Cloud Messaging](https://console.firebase.google.com/project/_/settings/cloudmessaging) tab of the Firebase console Settings pane.

```ruby
require 'qontak_fcm'

FCM_SERVER_KEY   = ENV['FCM_SERVER_KEY'].split("key=").last # Your server key
FCM_PROJECT_NAME = ENV['FCM_PROJECT_NAME']
FCM_GDC_PATH     = ENV['FCM_GDC_PATH']
$fcm_legacy_HTTP = QontakFcm.new(FCM_GDC_PATH, $redis_cache, FCM_PROJECT_NAME, "legacy_HTTP", server_key: FCM_SERVER_KEY, pool_size: 25)
$fcm_HTTP_v1     = QontakFcm.new(FCM_GDC_PATH, $redis_cache, FCM_PROJECT_NAME, "HTTP_v1", pool_size: 25)
device_token = "..." # The device token of the device you'd like to push a message to

payload = {
  to: device_token,
  notification: {
    title: "Update",
    body: "Your weekly summary is ready",
    click_action: "BLEH"
  },
  data: { extra: "data" }
}

response = $fcm_legacy_HTTP.push(payload)

headers = response.headers
headers['Retry-After'] # => returns 'Retry-After'

json = response.json
json[:canonical_ids] # => 0
json[:failure]       # => 0
json[:multicast_id]  # => 8478364278516813477

result = json[:results].first
result[:message_id]      # => "0:1489498959348701%3b8aef473b8aef47"
result[:error]           # => nil, "InvalidRegistration" or something else
result[:registration_id] # => nil
```

### Topic Messaging:

```ruby
topic   = "/topics/foo-bar"
payload = {
  to: topic,
  data: {
    message: "This is a Firebase Cloud Messaging Topic Message!",
  }
}
response = $fcm_legacy_HTTP.push(payload) # => sends a message to the topic
```

## Performance

The qontak-fcm gem uses [HTTP persistent connections](https://en.wikipedia.org/wiki/HTTP_persistent_connection) to improve performance. This is done by [the net-http-persistent gem](https://github.com/drbrain/net-http-persistent). A simple benchmark shows that the qontak-fcm gem performs at least 3x faster than the fcm gem (Kondone Loh, mik kondone lo ya):

```sh
$ ruby bench.rb
Warming up --------------------------------------
             qontak_fcm     2.000  i/100ms
                 fcm     1.000  i/100ms
Calculating -------------------------------------
             qontak_fcm     28.009  (± 7.1%) i/s -    140.000  in   5.019399s
                 fcm      7.452  (±13.4%) i/s -     37.000  in   5.023139s

Comparison:
             qontak_fcm:       28.0 i/s
                 fcm:        7.5 i/s - 3.76x  slower
```

## Contributing

No need!

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
