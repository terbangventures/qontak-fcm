# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'qontak_fcm/version'

Gem::Specification.new do |spec|
  spec.name          = 'qontak-fcm'
  spec.version       = QontakFcm::VERSION
  spec.authors       = ['terbangventures']
  spec.email         = ['namakukingkong@gmail.com']
  spec.summary       = 'Simple, fast, high-quality client for FCM (Firebase Cloud Messaging)'
  spec.description   = 'Android Push Notification in Ruby: simple, fast, high-quality client for FCM (Firebase Cloud Messaging)'
  spec.homepage      = 'https://bitbucket.org/terbangventures/qontak-fcm'
  spec.license       = 'MIT'
  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test)/}) }
  spec.require_paths = ['lib']

  spec.add_dependency 'net-http-persistent', '>= 3.0.0'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'minitest'
  spec.add_development_dependency 'rake'
end
